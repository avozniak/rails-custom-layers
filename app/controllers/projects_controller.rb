class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :edit, :update, :destroy]

  def index
    @projects = user_projects
  end

  def show
    # TODO: Refactor to policy
  end

  def new
    # TODO: Refactor to form
    @project = Project.new
  end

  def create
    # TODO: Refactor to form & policy
    @project = Project::Create.new(project_params, current_user).call

    if policy.allowed?
      @project.save
      redirect_to @project, notice: t('.notice')
    else
      redirect_to projects_path,
      alert: t('.alert', user_plan: current_user.plan)
    end
  end

  def update
    # TODO: Refactor to form
    if @project.update(project_params)
      redirect_to @project, notice: t('.notice')
    else
      render :edit
    end
  end

  def destroy
    @project.destroy
    redirect_to projects_url, notice: t('.notice')
  end

  private

  def user_projects
    Project::AllForUser.new(current_user).call
  end

  def set_project
    @project = current_user.projects.find(params[:id])
  end

  def project_params
    params.require(:project).permit(:name, :user_id)
  end

  def policy
    Project::CreatePolicy.new(current_user)
  end
end
