class Project
  class AllForUser
    # TODO: Implement
    attr_reader :user, :relation

    def initialize(user, relation = Project.all)
      @relation = relation
      @user = user
    end

    def call
      @relation.where(user_id: @user)
    end
  end
end
