class Project
  class CreatePolicy
    # TODO: Implement
    FREE_MAX_PROJECTS = 3
    BUSINESS_MAX_PROJECTS = 10
    PREMIUM_MAX_PROJECTS = 100

    def initialize(user)
      @user = user
    end

    def allowed?
      free? || business? || premium? || custom?
    end

    private

    def free?
      @user.plan == 'free' and @user.projects.count < FREE_MAX_PROJECTS
    end

    def business?
      @user.plan == 'business' and @user.projects.count < BUSINESS_MAX_PROJECTS
    end

    def premium?
      @user.plan == 'premium' and @user.projects.count < PREMIUM_MAX_PROJECTS
    end

    def custom?
      @user.plan == 'custom'
    end
  end
end
